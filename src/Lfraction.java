import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      valueOf("6/7");
      valueOf("         10/50      ");
      valueOf("-6/-7");
      //valueOf("6#/7");
      //valueOf("67");
      //valueOf("6/7/10");
      //valueOf("6#/7");
      //valueOf("67");
      //valueOf("6/7/10");
      //Lfraction test1 = new Lfraction(0, 10);
      //Lfraction test2 = new Lfraction(5, 10);
      //test1.inverse();
      //test2.divideBy(test1);
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if(b == 0){
         throw new RuntimeException("The denominator can not be 0");
      }
      long gcd = getGcd(a, b); // GCD stands for "greatest common divisor"
      this.numerator = a/gcd*(b/Math.abs(b));
      this.denominator = Math.abs(b/gcd);

   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + "/" + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if(compareTo((Lfraction) m) == 0 ){
         return true;
      }else{
         return  false;
      }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long commonden = this.denominator*m.getDenominator();
      long mnum = m.getNumerator()*this.denominator;
      long thisnum = this.numerator*m.getDenominator();
      long a = mnum + thisnum;
      long gcd = getGcd(a, commonden);

      return new Lfraction(a/gcd, commonden/gcd);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long a = this.numerator*m.getNumerator();
      long b = this.denominator*m.getDenominator();
      long gcd = getGcd(a, b);

      return new Lfraction(a/gcd, b/gcd);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.numerator == 0){
         throw new ArithmeticException("Unable to inverse fraction with 0 as numerator in fraction: " +this.numerator + "/" +this.denominator+ "! Would not be able to divide by zero!");
      }
      return new Lfraction(this.denominator, this.numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long commonden = this.denominator*m.getDenominator();
      long mnum = m.getNumerator()*this.denominator;
      long thisnum = this.numerator*m.getDenominator();
      long a = thisnum - mnum;
      long gcd = getGcd(a, commonden);

      return new Lfraction(a/gcd, commonden/gcd);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getNumerator() == 0){
         throw new ArithmeticException("Unable to divide fraction with a fraction with 0 as numerator in the second fraction. In expression " +this.numerator + "/" +this.denominator + " divided by " +m.getNumerator() + "/" + m.getDenominator() + " we would not be able to divide by zero!");
      }

      long a = this.numerator*m.getDenominator();
      long b = this.denominator*m.getNumerator();
      long gcd = getGcd(a, b);

      return new Lfraction(a/gcd, b/gcd);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long a = this.numerator*m.getDenominator();
      long b = this.denominator*m.getNumerator();

      if(a > b){
         return 1;
      }else if (a < b){
         return -1;
      }else{
         return 0;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator/this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction((this.numerator % this.denominator), this.denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.numerator/(double)this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long a = Math.round(f*d);
      return new Lfraction(a, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] nums = s.trim().split("/");

      if (s.trim().isEmpty()){
         throw new RuntimeException("The entered string was empty!");
      }

      if (!s.contains("/")) {
         throw new RuntimeException("No divider operator in expression " + s);
      }

      if (nums.length > 2){
         throw new RuntimeException("Expression " +s+ " is too long!");
      }

      try {
         Long.parseLong(nums[0]);
      }catch(Exception e){
         throw new RuntimeException("First element " + nums[0] + " can not be converted to type long in expression " + s);
      }

      try {
         Long.parseLong(nums[1]);
      }catch(Exception e){
         throw new RuntimeException("Second element " + nums[1] + " can not be converted to type long in expression " + s);
      }

      return new Lfraction(Long.parseLong(nums[0]), Long.parseLong(nums[1]));
   }

   //Viide, funktsioon osaliselt leitud siit: http://stackoverflow.com/questions/13673600/how-to-write-a-simple-java-program-that-finds-the-greatest-common-divisor-betwee
   public static long getGcd(long a, long b){
      if(a == 0 || b == 0) return Math.abs(a+b); // base case
      return Math.abs(getGcd(b,a%b));
   }
}

